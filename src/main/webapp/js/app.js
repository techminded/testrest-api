import $ from 'jquery';
import 'bootstrap';
import 'eternicode/bootstrap-datepicker/js/bootstrap-datepicker';


export class AppView {

    constructor(params) {
        this.data = {};
        this.el = $(params.el);
        this.editModal = this.el.find('.edit-modal');
        this.editModal.modal({ show: false });
        this.dateFormat = params.dateFormat ? params.dateFormat : 'yyyy-mm-dd';
        this.fields = {
            'firstName': { type: 'text', required: true},
            'secondName': { type: 'text', required: true},
            'lastName': { type: 'text', required: true},
            'birthDayString':  { type: 'date', required: true},
            'id':  { type: 'text', required: false}
        };
        this.grid = this.el.find('table tbody');
    }

    render() {
        this.loadUsers();
    }

    loadUsers() {
        $.ajax({
            url: '/testrest-api/resources/users',
            dataType: 'json',
            success: function(data) {
                for (var i in data) {
                    this.data[i] = data[i];
                    this.renderRow(i);
                }
                this.bindActions();
            }.bind(this)
        });
    }

    bindActions() {
        this.el.on('click', '.btn-add', function(event) {
            this.onAddClicked(event);
        }.bind(this));

        this.editModal.on('click', '.btn-primary', function(event) {
            this.onSaveClicked(event);
        }.bind(this));

        this.editModal.find('.datepicker').datepicker({
            format: this.dateFormat
        });

        this.grid.on('click', '.btn-edit', function(event) {
            this.onEditClicked(event);
        }.bind(this));

        this.grid.on('click', '.btn-delete', function(event) {
            this.onDeleteClicked(event);
        }.bind(this));
    }

    onAddClicked(event) {
        this.updateForm({});
        this.editModal.modal('show');
    }

    onEditClicked(event) {
        let id = $(event.target).attr('data-ref');
        let user = this.data[id];
        for (let fieldName of Object.keys(this.fields)) {
            this.editModal.find('#' + fieldName).val(user[fieldName]);
        }
        this.editModal.modal('show');
    }

    onSaveClicked(event) {
        if (! this.validate()) {
            alert("Fill the form properly, please");
        }
        var id = this.editModal.find('#id').val(),
            url = '/testrest-api/resources/users',
            requestType = 'post';
        id = parseInt(id);
        if (id > 0) {
            url = '/testrest-api/resources/users/' + id;
            requestType = 'put';
        }
        $.ajax({
            type: requestType,
            url: url,
            data: this.loadFormData(),
            dataType: 'json',
            success: function(data) {
                var id = parseInt(data.id);
                this.data[id] = data;
                if (requestType == 'post') {
                    this.renderRow(id);
                    this.updateForm({});
                } else {
                    this.updateRow(id);
                }
                this.editModal.modal('hide');
            }.bind(this)
        })
    }

    onDeleteClicked(event) {
        let id = $(event.target).attr('data-ref');
        $.ajax({
            url: '/testrest-api/resources/users/' + id,
            type: 'delete',
            dataType: 'json',
            success: function(data) {
                this.removeRow(id);
            }.bind(this),
            error: function() {
                alert('Error deleting user');
            }
        });
    }

    isValidDate(value) {
        var dateWrapper = new Date(value);
        return !isNaN(dateWrapper.getDate());
    }

    validate() {
        for (let fieldName of Object.keys(this.fields)) {
            var field = this.fields[fieldName],
                value = this.editModal.find('#' + fieldName).val();
            if (field.required) {
                if (value == undefined || value == '') {
                    return false;
                }
            }
            if (field.type == 'number') {
                if (! parseFloat(value)) {
                    return false;
                }
            }
            if (field.type == 'date') {
                if (! this.isValidDate(value)) {
                    return false;
                }
            }
        }
        return true;
    }

    updateForm(data) {
        for (let fieldName of Object.keys(this.fields)) {
            var value = data[fieldName] != undefined ? data[fieldName] : '';
            this.editModal.find('#' + fieldName).val(value);
        }
    }

    loadFormData() {
        var data = {};
        for (let fieldName of Object.keys(this.fields)) {
            data[fieldName] = this.editModal.find('#' + fieldName).val();
        }
        return data;
    }

    removeRow(id) {
        delete this.data[id];
        $('.row-user-' + id).html('');
    }

    renderRow(id) {
        let user = this.data[id];
        this.grid.append(
            `<tr class="row-user-${user.id}">
                <td class="row-user-id">${user.id}</td>
                <td class="row-user-firstName">${user.firstName}</td>
                <td class="row-user-secondName">${user.secondName}</td>
                <td class="row-user-lastName">${user.lastName}</td>
                <td class="row-user-birthDayString">${user.birthDayString}</td>
                <td class="actions">
                    <button type="button" data-ref="${user.id}" class="btn-edit">Edit</button>
                    <button type="button" data-ref="${user.id}" class="btn-delete">Delete</button>
                </td>
            </tr>`
        );
    }

    updateRow(id) {
        var rowEl = this.grid.find('.row-user-' + (id).toString()),
            data = this.data[id];
        if (rowEl != undefined && data != undefined) {
            for (let fieldName of Object.keys(this.fields)) {
                var fieldEl = rowEl.find('.row-user-' + fieldName);
                fieldEl.html(data[fieldName]);
            }
        }

    }
}