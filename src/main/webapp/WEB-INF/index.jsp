<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/jspm_packages/github/twbs/bootstrap@3.3.5/css/bootstrap.css" />
</head>
<body>

<div id="app">
    <div class="container">
        <div class="row">
            <button type="button" class="btn btn-add">Add</button>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Second Name</th>
                        <th>Last Name</th>
                        <th>Birthday</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade edit-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">User</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="firstName" class="control-label">First Name</label>
                        <input type="text" required="required" class="form-control" id="firstName" name="firstName" />
                    </div>
                    <div class="form-group">
                        <label for="secondName" class="control-label">Second Name</label>
                        <input type="text" required="required" class="form-control" id="secondName" name="secondName" />
                    </div>
                    <div class="form-group">
                        <label for="lastName" class="control-label">Last Name</label>
                        <input type="text" required="required" class="form-control" id="lastName" name="lastName" />
                    </div>
                    <div class="form-group">
                        <label for="birthDayString" class="control-label">Birthday</label>
                        <input type="date" required="required" class="datepicker form-control" id="birthDayString"  name="birthDayString" />
                        <input type="hidden" id="id" name="id" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

</div>


<script src="<%= request.getContextPath() %>/jspm_packages/system.js"></script>
<script src="<%= request.getContextPath() %>/config.js"></script>
<script>
    Promise.all([
        System.import('js/app')
    ]).then(function(modules) {
        var view = new modules[0].AppView({
            el: '#app'
        }).render();

    }, console.error.bind(console));
</script>
</body>
</html>