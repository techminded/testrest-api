package com.testrest.entity;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class User {

    private long id;

    private String firstName;

    private String secondName;

    private String lastName;

    private LocalDate birthDay;

    public User() {
    }

    public User(long id, String firstName, String secondName, String lastName, LocalDate birthDay) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
        this.birthDay = birthDay;
    }

    public String getBirthDayString() {
        final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return getBirthDay().format(dateFormatter);
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }
}
