# TestRest Api #

## Installation ##

You should have maven3, java 8, nodejs and npm installed and optionally java IDE and wildfly servlet container. Checkout project and do the following:

```
#!bash

cd testrest-api/src/main/webapp/
npm intall jspm
./node_modules/.bin/jspm install
```

This should download all required js dependencies. And if you're lucky one, you can import project (as maven) in your favorite IDE and run with Wildfly server, or build a package with:
```
#!bash
mvn package
```

and deploy it manually