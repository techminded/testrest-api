package com.testrest.resource;


import com.testrest.dao.UserDAO;
import com.testrest.entity.User;

import javax.faces.bean.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import java.util.HashMap;
import java.util.Map;


@Named
@Path("users")
@ApplicationScoped
public class UserResource {

    @Inject
    UserDAO userDao;

    @GET
    @Path("/")
    @Produces({MediaType.APPLICATION_JSON})
    public Response users() {
        Map<Long, User> users = userDao.fetchAll();
        GenericEntity<Map<Long, User>> entity = new GenericEntity<Map<Long, User>>(users) {};
        return Response.ok(entity).build();
    }

    @POST
    @Path("/")
    @Produces({MediaType.APPLICATION_JSON})
    public Response create(MultivaluedMap<String, String> params) {
        User user = userDao.create(params);
        GenericEntity<User> entity = new GenericEntity<User>(user) {};
        return Response.ok(entity).build();
    }

    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response users(@PathParam("id") Long id) {
        User user = userDao.findOne(id);
        GenericEntity<User> entity = new GenericEntity<User>(user) {};
        return Response.ok(entity).build();
    }

    @PUT
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response users(@PathParam("id") Long id, MultivaluedMap<String, String> params) {
        User user = userDao.updateFromParams(userDao.findOne(id), params);
        GenericEntity<User> entity = new GenericEntity<User>(user) {};
        return Response.ok(entity).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response delete(@PathParam("id") Long id) {
        userDao.delete(id);
        Map<String, String> response = new HashMap<>();
        response.put("state", "ok");
        GenericEntity<Map<String, String>> entity = new GenericEntity<Map<String, String>> (response) {};
        return Response.ok(entity).build();
    }

}
