package com.testrest.dao;

import com.testrest.entity.User;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.ws.rs.core.MultivaluedMap;

@Named
@SessionScoped
public class UserDAO implements Serializable {

    private Map<Long, User> users = new LinkedHashMap<>();

    private Long counter = 0L;

    final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public Long incrementCounter() {
        return ++this.counter;
    }

    public User findOne(long id) {
        return users.get(id);
    }

    public Map<Long, User> fetchAll() {
        return this.users;
    }

    public User create(MultivaluedMap<String, String> params) {
        long userId = this.incrementCounter();
        User user = new User();
        user.setId(userId);
        user = this.updateFromParams(user, params);
        return user;
    }

    public User save(User user) {
        this.users.put(user.getId(), user);
        return user;
    }

    public void delete(long id) {
        if (this.users.containsKey(id)) {
            this.users.remove(id);
        }
    }

    public User updateFromParams(User user, MultivaluedMap<String, String> params) {
        LocalDate birthDay = LocalDate.parse(params.getFirst("birthDayString"), dateFormatter);
        user.setBirthDay(birthDay);
        user.setFirstName(StringEscapeUtils.escapeHtml4(params.getFirst("firstName")));
        user.setSecondName(StringEscapeUtils.escapeHtml4(params.getFirst("secondName")));
        user.setLastName(StringEscapeUtils.escapeHtml4(params.getFirst("lastName")));
        user = this.save(user);
        return user;
    }
}